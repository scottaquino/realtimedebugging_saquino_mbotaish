/*
Memory Management Playground (MMP)
By Daniel S. Buckstein
Editted by Mark Botaish and Scott Aquino with permission from the author
Copyright 2019-2020
*/
//-----------------------------------------------------------------------------

#include "mmp/mmp_memory.h"

#pragma comment(lib, "MemoryManagementPlayground.lib")


//-----------------------------------------------------------------------------

typedef		byte				chunk_kb[1024];


//-----------------------------------------------------------------------------

#define		decl_argc			ui32 const argc
#define		decl_argv			cstrp const argv[]
typedef		i32(*entry_func)(decl_argc, decl_argv);


//-----------------------------------------------------------------------------

int testMMP(decl_argc, decl_argv);
int testMalloc(decl_argc, decl_argv);


//-----------------------------------------------------------------------------

int main(decl_argc, decl_argv)
{
	//return testMMP(argc, argv);
	return testMalloc(argc, argv);
}


//-----------------------------------------------------------------------------

int testMMP(decl_argc, decl_argv)
{
	// stack-allocate a bunch of data
	chunk_kb chunk[12];
	size count = sizeof(chunk);
	ptr chunk_base = mmp_set_zero(chunk, count);



	// done, stack-allocated data popped
	return 0;
}


//-----------------------------------------------------------------------------

#include <stdlib.h>


int testMalloc(decl_argc, decl_argv)
{
	// stack-allocate a bunch of data
	chunk_kb chunk[12];
	size count = sizeof(chunk);
	ptr chunk_base = mmp_set_zero(chunk, count);

	ptr pool_00_05 = mmp_pool_init(chunk, count / 2, count);

	ptr block = mmp_block_reserve(pool_00_05, 10);
	ptr block2 = mmp_block_reserve(pool_00_05, 10);



	mmp_block_release(block, pool_00_05);

	size* fuck = (size*)(chunk[0][0]);
	union forFuckSakes testing = {0};
	mmp_copy(&testing, (char*)pool_00_05 + 2, sizeof(forFuckSakes));
	count = mmp_pool_term(pool_00_05);

	return 0;

}
