#include "mmp/cPointer.h"

int main() 
{

	//defauly ctor
	cDummy obj;

	//copy ctor
	cDummy obj2 = obj;

	// assigne op
	obj = obj2;

	int val = obj->stuff;

	//Every constructor needs a destructor
	char* buffer = new char[1024];
	cDummy* obj3 = new(buffer)cDummy(3);//never delete placement new . Construction wo/ allocation

	obj3->~cDummy();
	obj3 = 0;
	delete[] buffer;

	//dtor called 


	//cNonVoidTest<int> testNonVoid;
	//testNonVoid.somefunc(3);
	//
	//cNonVoidTest<void> testVoid;
	//testVoid.somefunc(3);
}