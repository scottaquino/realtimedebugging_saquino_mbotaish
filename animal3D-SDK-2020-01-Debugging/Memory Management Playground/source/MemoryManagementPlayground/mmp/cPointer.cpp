#include "mmp/cPointer.h"

cDummy::cDummy() {
	data = 0;
	size = 0;
}

cDummy::cDummy(cDummy const& rhs) 
	:cDummy(rhs.size)
{
	//copy the array 
	if (data) {

	}
}

cDummy::cDummy(cDummy&& rhs) {

	size = rhs.size;
	data = rhs.data;
	rhs.data = 0;
}

cDummy::~cDummy() {
	if (data) {
		delete[] data;
	}
}

cDummy::cDummy(size_t newSize)
	:cDummy()
{
	if (newSize > 0) {
		size = newSize;
		data = new blah[newSize];
	}
	
}

cDummy& cDummy::operator=(cDummy const& rhs) {

	if (data) {
		size = 0;
		delete[] data;
	}
	if (rhs.data) {
		size = rhs.size;
		data = new blah[size];

		//copy data
	}
	return *this;
}

cDummy& cDummy::operator=(cDummy&& rhs) {

	if (data) {
		size = 0;
		delete[] data;
	}

	size = rhs.size;
	data = rhs.data;
	rhs.data = 0;

	return *this;
}

//new/delete
void* operator new (size_t sz) {
	//return ::operator new(sz);
	return 0;
}

void operator delete(void* ptr) {
	//::operator delete(ptr);
}

void* operator new(size_t sz, void* buffer) {
	//return buffer;
	return 0;
}

void operator delete(void* buffer, void* ptr) {

}
//new/delete
void* operator new[] (size_t sz) {
	//return ::operator new(sz);
	return 0;
}

void operator  delete[](void* ptr) {
	//::operator delete(ptr);
}

void* operator new[](size_t sz, void* buffer) {
	//return buffer;
	return 0;
}

void operator delete[](void* buffer, void* ptr) {

}

