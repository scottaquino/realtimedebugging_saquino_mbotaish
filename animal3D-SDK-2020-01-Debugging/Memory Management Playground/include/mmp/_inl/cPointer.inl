#ifdef _CPOINTER_H_
#ifndef _CPOINTER_INL_
#define _CPOINTER_INL_

// pass-thrus custom placement new/delete
template <typename bufferType >
inline void* operator new(size_t sz, bufferType* buffer) {
	// DO whatever management buffer does 
	//void* ret = buffer->alloc(sz);
	void* p = 0;
	return p;
}

template <typename bufferType >
inline void operator delete(void* p, bufferType* buffer) {
	//buffer->dealloc(p);
}

//Array new always call default constructor by default. This prevents that
template <typename bufferType >
inline void* operator new[](size_t sz, bufferType* buffer) {
	//return operator::new<bufferType>(sz, buffer);
	return 0;
}

template <typename bufferType >
inline void operator delete[](void* p, bufferType* buffer) {
	//operator::operator delete<bufferType>(p, buffer)
}


#endif // !_CPOINTER_INL_
#endif // _CPOINTER_H_