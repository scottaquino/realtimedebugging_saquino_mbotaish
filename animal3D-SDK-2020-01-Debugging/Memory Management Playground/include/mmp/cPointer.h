#ifndef _CPOINTER_H_
#define _CPOINTER_H_

template<bool , typename T = void>
struct TYPEDEF {};
template<typename T>
struct TYPEDEF<true, T> { typedef T TYPE; };

#define GETTYPE(T)			TYPEDEF<T != void>::TYPE
#define NONVOID(T)			template<typename t = GETTYPE(T)>


template <typename T>
class cNonVoidTest {
public:
	NONVOID(T) t somefunc(int num) {}
};


struct blah {
	int stuff;
};

class cDummy {

	blah* data;
	size_t size;

public:
	
	cDummy();//default ctor
	cDummy(cDummy const& copy); //copy ctoy
	cDummy(cDummy&& rhs); //move ctor

	~cDummy(); //dtor

	// "param" ctor
	cDummy(size_t newSize);

	cDummy& operator=(cDummy const& rhs); //copy assignment
	cDummy& operator=(cDummy&& rhs); // move assignment. && - rvalue (temp data)
	/*
	cDummy& operator=(cDummy const& rhs) = default; // what is would normally do
	cDummy& operator=(cDummy const& rhs) = delete; //can not do the copy assigment
	*/

	// accessor operator
	blah* operator->() const { return data; }

	
};

class cPointer {

};

//new/delete
void* operator new (size_t sz);
void* operator new[](size_t sz);
void operator delete(void* ptr);
void operator delete[](void* ptr);
void* operator new(size_t sz, void* buffer);
void* operator new[](size_t sz, void* buffer);
void operator delete(void* buffer, void* ptr);
void operator delete[](void* buffer, void* ptr);



#include "_inl/cPointer.inl"

#endif // _CPOINTER_H_

